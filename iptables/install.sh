#!/bin/bash

## Port used for ssh
SSH_PORT="1805"

# Path to the service file
SERVICE_PATH="/etc/systemd/system/iptables.service"
CONFIG_PATH="/etc/iptables"

#Where to store saved rules
SAVE_PATH="${CONFIG_PATH}/iptables.save"
STOP_PATH="${CONFIG_PATH}/iptables_stop.sh"

## Fill is_* vars if true, leave empty if not
is_dns=""
is_serveur="yes"

## Used to configure local connections on specific interface, between server and raspberry
#local_network="yes"
#local_interface="enp3s1"
#local_partner_ip="192.168.1.100"


## Used to allow local trafic for cloud server
traffic="yes"
traffic_port_list="1212 1313"
traffic_local_network="192.168.0.0/24"

# Empty current tables
./iptables_stop.sh

# Default policy
iptables -t filter -P INPUT DROP
iptables -t filter -P FORWARD DROP
iptables -t filter -P OUTPUT DROP

# Enbale loopback
iptables -t filter -A INPUT -i lo -j ACCEPT
iptables -t filter -A OUTPUT -o lo -j ACCEPT

# Enable established connections
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

# Authorize pings
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
iptables -A INPUT -p icmp --icmp-type time-exceeded -j ACCEPT
iptables -A INPUT -p icmp --icmp-type destination-unreachable -j ACCEPT
iptables -A OUTPUT -p icmp -j ACCEPT

# Enable ssh
iptables -t filter -A INPUT -p tcp --dport $SSH_PORT -j ACCEPT
iptables -t filter -A OUTPUT -p tcp --dport $SSH_PORT -j ACCEPT

# Allow internet output:
#Autoriser DNS
iptables -A OUTPUT -p udp --dport 53 -j ACCEPT
iptables -A INPUT -p udp --dport 53 -j ACCEPT

#Autoriser sortie FTP
iptables -A OUTPUT -p tcp --dport 21 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 20 -j ACCEPT
iptables -A OUTPUT -p udp --dport 161 -j ACCEPT
iptables -A INPUT -p udp --dport 161 -j ACCEPT

echo "Basic rules created"

# Enable traffic on specific port for local network (only four cloud server)
if [ -n "$traffic" ] ; then
	for port in $traffic_port_list ; do
		echo "Traffic enabled on network "$traffic_local_network" with port $port"
		iptables -t filter -A INPUT -p tcp --dport $port -s $traffic_local_network -j ACCEPT
	done	
fi

# Enable ssh output port
if [ -n "$SSH_OUTPUT_PORT" ] ; then
	echo "SSH is allowed on port $SSH_OUTPUT_PORT"
	iptables -t filter -A OUTPUT -p tcp --dport $SSH_OUTPUT_PORT -j ACCEPT
fi

# Enable http and https
if [ -n "$is_serveur" ] ; then
	echo "HTTP and HTTPS traffic is allowed"
	# HTTP + HTTPS Out
	iptables -t filter -A OUTPUT -p tcp --dport 80 -j ACCEPT
	iptables -t filter -A OUTPUT -p tcp --dport 443 -j ACCEPT
	# HTTP + HTTPS In
	iptables -t filter -A INPUT -p tcp --dport 80 -j ACCEPT
	iptables -t filter -A INPUT -p tcp --dport 443 -j ACCEPT
	iptables -t filter -A INPUT -p tcp --dport 8443 -j ACCEPT
fi

# DNS In/Out
if [ -n "$is_dns" ] ; then
	echo "DNS server traffic is allowed"
	iptables -t filter -A OUTPUT -p tcp --dport 53 -j ACCEPT
	iptables -t filter -A OUTPUT -p udp --dport 53 -j ACCEPT
	iptables -t filter -A INPUT -p tcp --dport 53 -j ACCEPT
	iptables -t filter -A INPUT -p udp --dport 53 -j ACCEPT
fi

## Allow local network comunications
if [ -n "$local_network" ] ; then
	echo "Network between two specific partners is allowed  on $local_interface, with $local_partner_ip"
	iptables -t filter -A INPUT -p tcp  -s $local_partner_ip -i $local_interface -j ACCEPT
	iptables -t filter -A OUTPUT -p tcp -d $local_partner_ip -o $local_interface -j ACCEPT
fi

## Setup service
mkdir -p /etc/iptables
cp ./iptables_stop.sh $STOP_PATH

if [ ! -e "$SERVICE_PATH" ] || [ "/root/scripts/iptables/iptables.service" -nt $SERVICE_PATH ] ; then
	cp ./iptables.service $SERVICE_PATH
	systemctl enable iptables
	systemctl start iptables
	iptables-save > $SAVE_PATH
	echo "Service had been updated"
else
	echo "Service was up to date"
fi

