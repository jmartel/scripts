#!/bin/bash

log_file="/var/log/scripts/auto_shutdown.log"

date '+%Y-%m-%d %H:%M:%S' >> $log_file

users=`users`
if [ -n "$users" ] ; then
	echo "$users is/are logged, can't stop server" >> $log_file
	echo -e "\n" >> $log_file
else
	echo "Automatic shutdown" >> $log_file
	echo "uptime : " 'uptime -p' >> $log_file
	echo -e "\n" >> $log_file
	shutdown -t 0
fi

