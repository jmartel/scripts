#!/bin/bash

name="auto_shutdown"
cron_file="/etc/cron.d/${name}"
log_file="/var/log/scripts/${name}.log"
script_file="/root/scripts/${name}/${name}.sh"

if [ ! -e "$cron_file" -o "./install.sh" -nt $cron_file ] ; then
	echo "Updating cron task in : $cron_file"
	touch $cron_file
	echo 'SHELL=/bin/bash'  > $cron_file
	echo 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin' >> $cron_file
	echo '' >> $cron_file
	echo "00 20 * * * root $script_file" >> $cron_file
	echo "00 22 * * * root $script_file" >> $cron_file
	echo "00 00 * * * root $script_file" >> $cron_file
	echo "0 0/2 * * * root $script_file" >> $cron_file
else
	echo "Cron task already exists in $cron_file"
fi

echo "Cleaning log file : $log_file"
rm $log_file
touch $log_file
