#!/bin/bash

cron_file="/etc/cron.d/server_watchdog"
script_file="/root/scripts/watchdog/server_watchdog.sh"
log_file="/var/log/scripts/server_watchdog.log"

if [ ! -e $cron_file -o "./install.sh" -nt "$cron_file" ] ; then
	echo "Cron task not found, creating file in $cron_file"
	touch $cron_file
	echo 'SHELL=/bin/bash'  > $cron_file
	echo 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin' >> $cron_file
	echo '' >> $cron_file
	echo "@reboot root $script_file" >> $cron_file
	echo "00 08 * * * root $script_file" >> $cron_file
else
	echo "Cron task already exists in $cron_file"
fi

echo "Cleaning log file : $log_file"
rm $log_file
touch $log_file
