#!/usr/bin/python3

import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(3, GPIO.OUT)

def short():
    GPIO.output(3, GPIO.LOW)
    time.sleep(1)
    GPIO.output(3, GPIO.HIGH)

def long():
    GPIO.output(3, GPIO.LOW)
    time.sleep(5)
    GPIO.output(3, GPIO.HIGH)

def restart():
    long()
    time.sleep(5)
    short()

restart()
