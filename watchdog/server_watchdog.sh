#!/bin/bash

## file to write logs
logfile="/var/log/scripts/server_watchdog.log"

## Pqth for python scripts used to control the server button
on_script="/usr/scripts/server_short.py"
restart_script="/usr/scripts/server_restart.py"

## Time to wait before button pushes and new pings
sleep_time="150"

## Hour between server is suposed to work
start_hour=8
stop_hour=20

## Ping server, using local network between them, return 0 for success
ping_server ()
{
	ping -c 1 server_local 1>/dev/null 2>/dev/null
	res=$?
	return $res
}

## Short button press, before sleep, return the ping_server res
start ()
{
	$on_script
	sleep $sleep_time
	ping_server
	return $?
}

## Long and short button press, before sleep, return the ping_server res
restart ()
{
	$restart_script
	sleep $sleep_time
	ping_server
	return $?
}

date '+%Y-%m-%d %H:%M:%S' >> $logfile

## Checking we are in working time for server
hour=`date '+%H'`
if [ $hour -lt $start_hour ] || [ $hour -gt $stop_hour ] ; then
	echo "We are not in the server working time"
	echo "Exiting"
	exit 0
fi

ping_server

if [ $? -eq 0 ] ; then
	echo "Server is online" >> $logfile
else
	echo "Server is offline, trying to start" >> $logfile
	start
	if [ $? -eq 0 ] ; then
		echo "Server started" >> $logfile
	else
		echo "Server do not started" >> $logfile
		echo "Trying to reboot" >> $logfile
		restart
		if [ $? -eq 0 ] ; then
			echo "Server successfully rebooted" >> $logfile
		else
			echo "Server do not started !!!!!!!!!!!" >> $logfile
		fi
	fi
fi

echo "" >> $logfile
