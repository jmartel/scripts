#!/bin/bash

log_dir="/var/log/scripts"

if [ ! -e $log_dir ] ; then
	mkdir $log_dir
fi

for dir in `ls .` ; do
	if [ -d $dir ] ; then 
		echo "Do you want to install $dir ?"
		echo "Y/N"
		read val
		if [ "$val" = "Y" ] ; then
			cd $dir
			echo "Installing $dir"
			./install.sh
			cd ..
		else
			echo "Aborted"
		fi
		echo -e "\n"
	fi
done
