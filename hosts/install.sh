#!/bin/bash

if [ `grep -c "My own entries" /etc/hosts` -eq 0 ] ; then
	echo "Added new entries in /etc/hosts"
	<./hosts cat >> /etc/hosts
else
	echo "Personnalized entries already exists in /etc/hosts"
fi
