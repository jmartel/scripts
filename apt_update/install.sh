#!/bin/bash

cron_file="/etc/cron.d/apt_update"
log_dir="/var/log/scripts/"
log_file="${log_dir}/apt_update.log"
script_file="/root/scripts/apt_update/apt_update.sh"

if [ ! -e "$cron_file" -o "./install.sh" -nt "$cron_file" ] ; then
	echo "Updating cron task in :  $cron_file"
	touch $cron_file
	echo 'SHELL=/bin/bash'  > $cron_file
	echo 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin' >> $cron_file
	echo '' >> $cron_file
	echo "@reboot root $script_file" >> $cron_file
	echo "00 03 * * * root $script_file" >> $cron_file
else
	echo "Cron task already exists in $cron_file"
fi

mkdir -p $log_dir
echo "Cleaning log file : $log_file"
rm -f $log_file
touch $log_file
