#!/bin/bash

log_file="/var/log/scripts/apt_update.log"

date '+%Y-%m-%d %H:%M:%S' >> $log_file

apt update  -y >> $log_file
apt upgrade -y >> $log_file

echo -e "\n" >> $log_file
